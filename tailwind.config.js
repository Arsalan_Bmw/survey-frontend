module.exports = {

    theme: {
      extend: {},
      fontFamily: {
        display:['"Source Sans Pro", sans-serif', 'Roboto'],
        body:['"Source Sans Pro" ,sans-serif','-apple-system','Segoe UI', 'Roboto']
      },
      minHeight: {
          '5' :'5vh',
          '95':'95vh',
          'screen' : '100vh',
          'full': '100%',
          '0': '0'
      },
      borderRadius: {
        'none': '0',
        'sm': '.125rem',
        default: '.25rem',
        'lg': '.5rem',
        'xl' :'1rem',
        'full': '9999px',
        'large': '12px',
      },
      backgroundColor: theme => ({
        ...theme('colors'),
        'primary': '#3289f7',
        'default': '#eff5fb',
      }),
      borderColor: theme => ({
        ...theme('colors'),
        'default': theme('colors.gray.300'),
        'primary': '#3289f7',
      })
    },
    variants: {
      tableLayout: ['responsive', 'hover', 'focus'],
    },
    plugins: []
  }
