# Ownerchat Frontend

## Requirements
- Latest or LTS Nodejs + NPM

## Installation
- Run npm install
- copy `.env.example` in your clipboard
- create `.env` file and paste the clipboard
- configure your local server inside `.env` file
- run local server using `npm run dev`