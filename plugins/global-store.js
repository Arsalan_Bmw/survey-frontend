export default ({app}, inject) => {
    inject('auth', app.store.state.auth);
}