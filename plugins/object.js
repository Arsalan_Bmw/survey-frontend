/**
 * Pick selected object by given keys
 *
 * @param {object} obj
 * @param {array} keys
 * @return {object}
 */
function only(obj, keys) {
    return keys.map( k => k in obj ? { [k]: obj[k] } : {} )
        .reduce((res, o) => Object.assign(res, o), {});
}

/**
 * Pick slected object except the given keys
 *
 * @param {object} obj
 * @param {array} keys
 * @return {object}
 */
function except(obj, keys) {
    const vkeys = Object.keys(obj).filter(k => !keys.includes(k));

    return only(obj, vkeys);
}

export default ({app}, inject) => {
    inject('object', { only, except });
}
