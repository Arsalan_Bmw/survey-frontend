import Vue from 'vue';

/**
 * Get first character of each string separate with space
 *
 * and uppercase it
 */
Vue.filter('first_string',function(value) {
    let strings = value.split(' ');

    let firstStrings = strings.map(string => {
        return string.charAt(0).toUpperCase()
    })

    return firstStrings.join('');
});

/**
 * remove underscore
 */
Vue.filter('no_underscore', function(value) {
    return value.replace('_',' ');
})

Vue.filter('uppercase', (value) => value.toUpperCase());

Vue.filter('capitalize', (value) => value.charAt(0).toUpperCase() + value.slice(1));