import Vue from 'vue';

import Error from '@/components/global/Error.vue';
import Pagination from '@/components/global/Pagination.vue';

Vue.component('error',Error);
Vue.component('oc-pagination', Pagination);