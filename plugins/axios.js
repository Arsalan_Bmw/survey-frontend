/**
| -----------------------------------------------------------------------------------------------------
| This plugin contain 2 axios instance, for auth who user has token inside the cookie and for guest user
| the default axios that provide from nuxt will be add loading every request,
| and hide the loading if the response is success or fail
| -----------------------------------------------------------------------------
*/

import axios from 'axios';
import Cookie from 'js-cookie';

export default function ({$axios, req, store, error, app},inject) {

    $axios.onRequest(config => {
        store.commit('errors/remove');

        if(!process.server) {
            document.querySelector('.lds-ring').classList.add('show');
        }
    })

    $axios.onResponse((response) => {
        if(!process.server) {
            document.querySelector('.lds-ring').classList.remove('show');
        }

        if(response.data && response.data.flash) {
            UIkit.notification({
                message: response.data.flash,
                pos: 'bottom-right',
                status: 'success',
            });
        }
    });

    $axios.onError((err) => {
        if(!process.server) {
            document.querySelector('.lds-ring').classList.remove('show');
        }

        if(err.response.status == 500) {
            error({statusCode: 500, message: 'Something went wrong, please reload the page or contact us'});
        }

    });
    const axiosAuth = axios.create({
        baseURL: process.env.BASE_URL
    });

    const axiosGQL = axios.create({
        baseURL: process.env.BASE_URL + '/api',
    })

    axiosAuth.interceptors.request.use(function(config) {
        store.commit('errors/remove');
        return config;
    });

    if(process.server && req.headers.cookie) {
        let token = app.$cookies.get('octoken');

        $axios.setToken(token,'Bearer');

        axiosAuth.defaults.headers.common['Authorization'] = `Bearer ${token || null}`;
        axiosGQL.defaults.headers.common['Authorization'] = `Bearer ${token || null}`;
    } else {
        $axios.setToken(Cookie.get('octoken') || null,'Bearer');

        axiosAuth.defaults.headers.common['Authorization'] = `Bearer ${Cookie.get('octoken') || ''}`
        axiosGQL.defaults.headers.common['Authorization'] = `Bearer ${Cookie.get('octoken') || ''}`
    }
    inject('axiosAuth', axiosAuth);
    inject('axiosGQL', axiosGQL);
}