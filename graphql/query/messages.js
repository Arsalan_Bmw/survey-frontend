export const getMessageStats = () => {
    return {
        query: `
            {
                buyer_owner_chat_messages {
                    data {
                        id
                    }
                }
                buyer_owner_chats {
                    data {
                        id
                    }
                }
            }
        `
    }
}