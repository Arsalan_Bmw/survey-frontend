export const campaignsAndSequenceTypes = () => {
    return {
        query: `
            {
                templates {
                    id
                    name
                    body
                    subject
                    type
                }
            }
        `
    }
}