# COMPONENTS

If you need to split components inside pages/ folder, you can use this components folder to split it all.

## rules
create subdir as same as pages folder

e.g for `/pages/companies/create.vue`, you should create subfolder
```
/components/companies/create/Form.vue

/components/companies/create/FormInformation.vue
```

or if the components can be use for more than 1 pages, e.g /create and /edit. just place the component like this

```
/components/companies/Form.vue
```

note: All Component name should be Capitalize ( Title Case)