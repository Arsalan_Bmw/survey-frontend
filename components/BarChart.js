import { line } from 'vue-chartjs'

export default {
  extends: line,
  props: ['data', 'options'],
  mounted () {
    setTimeout(function(){
    this.renderChart(this.data, this.options)
    }.bind(this),3000);
  }
}
