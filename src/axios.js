import axios from 'axios';
import router from './router'
//import status from 'status';
var join = require('url-join');
var isAbsoluteURLRegex = /^(?:\w+:)\/\//;
axios.interceptors.request.use((config) => {
  // Do something before request is sent
  if (!isAbsoluteURLRegex.test(config.url)) {
    //config config.url is api endpoint
    console.log(config.url);
    config.url = join('http://api.ownerchat.com/api/v1/', config.url);
    // config.url = join('http://textowner.local/api/v1/', config.url);
  }
  //  global header configuration
  config.headers = { Authorization: localStorage.getItem('token'), Accept: 'application/json' };
  return config;
}, (error) => {
  return Promise.reject(error);
});
// Add a response interceptor
axios.interceptors.response.use((response) => {
  // Do something with response data
  return response;
}, (error) => {
  // Do something with response error
  console.log(error.response.status);
  if (error.response.status == 401) {
    localStorage.removeItem('token');
    router.push('/login');
  }
  return Promise.reject(error);
});
export default axios;
