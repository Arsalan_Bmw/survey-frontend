import Vue from 'vue'
import CoreuiVue from '@coreui/vue'
import App from './App'
import router from './router/index'
import { iconsSet as icons } from './assets/icons/icons.js'

/*define gloabl axios*/
import axios from "./axios"
Vue.prototype.$http = axios;
Vue.use(axios)
import VueAlertify from 'vue-alertify';

Vue.use(VueAlertify);


import * as VueGoogleMaps from "vue2-google-maps";
Vue.use(VueGoogleMaps, {
    load: {
        key: "AIzaSyDt5wjyMwrXH7skTyPyFn-JoLqh_pO5YZU",
        libraries: "places" // necessary for places input
    }
});
// Vue.prototype.$http = axios
import babelPolyfill from 'babel-polyfill'
/*Sweet alerts toast*/
import Swal from 'sweetalert2'
window.Swal = Swal;
const toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000
});
window.toast = toast;

/**
 * End Sweet Alert
 */

Vue.use(CoreuiVue)




/**
 * Moment .js for date
 */

import moment from "moment";

/**
 * end moment .js--------
 */

Vue.filter('Date', function(created) {

    return moment(created).format('MMMM Do YYYY');
});

import InputTag from 'vue-input-tag'
Vue.component('input-tag', InputTag)

//checkboxes
import PrettyCheckbox from 'pretty-checkbox-vue';
Vue.use(PrettyCheckbox);

//bootstrap-vue
import BootstrapVue from 'bootstrap-vue'
Vue.use(BootstrapVue)

router.beforeEach((to, from, next) => {


    let permission = to.meta.permission;
    if (to.meta.auth) {

        if (localStorage.getItem('token')) {








            let userType = JSON.parse(localStorage.getItem('userDetail')).type;
            // if (userType == 'owner') {
            //     if (to.meta.company) {
            //         if (!localStorage.getItem('company_id') || localStorage.getItem('company_id') == 0) {
            //             next('select-company');
            //         }
            //     }
            // }


            if (userType && permission.includes(userType)) {
                console.log('user type match');
                next();
            } else {
                console.log('typ not matched')
                next('dashboard');
            }
        } else {
            console.log('token not exist');
            next('login');
        }
    } else {
        if (localStorage.getItem('token')) {
            if (to.name == 'login') {
                next('dashboard');
            }
        }
        next(); // This is where it should have been
    }
    // next(); - This is in the wrong place
});

router.beforeEach((to, from, next) => {

    const nearestWithTitle = to.matched.slice().reverse().find(r => r.meta && r.meta.title);

    if (nearestWithTitle) document.title = nearestWithTitle.meta.title;

    next();
});
/**
 * End Vue Router
 */

new Vue({
    el: '#app',
    mode: 'history',
    router,
    //CIcon component documentation: https://coreui.io/vue/docs/components/icon
    icons,
    template: '<App/>',
    components: {
        App
    }
})