import Vue from 'vue'
import Router from 'vue-router'

// Containers
// const TheContainer = () =>
//     import ('@/containers/TheContainer')
import TheContainer from '../containers/TheContainer';
// Views
// const Dashboard = () =>
//     import ('@/views/Dashboard')
import Dashboard from '../views/Dashboard';
const Charts = () =>
    import ('@/views/charts/Charts')
import EditCompanyUser from '../views/companies/editcompanyuser.vue';

import Owner from '../views/owner/owner.vue';
import AddProduct from '../views/owner/addproduct.vue';
import Lead from '../views/leads/lead.vue';
import SearchKeywords from '../views/searchkeywords/SearchKeywords';
import Buyer from '../views/buyer/buyer.vue';
import OwnerBuyer from '../views/leads/ownerbuyer.vue';
import Product from '../views/product/product.vue';
import AllProduct from '../views/product/AllProducts.vue';
import CompanyAllProducts from '../views/product/CompanyAllProducts.vue';
import Companies from '../views/companies/companylisting.vue';
import CreateCompany from '../views/companies/createcompany.vue';
import CreateCompanyUser from '../views/companies/createcompanyuser.vue'
import CompanyAdmins from '../views/companies/companyadmins.vue'

import Embed from '../views/embed/embed.vue';

import CreateProductTypes from '../views/productTypes/CreateProductTypes.vue'
import ProductTypes from '../views/productTypes/ProductTypes.vue'
import CreateProductTypeFields from '../views/productTypes/CreateProductTypeFields.vue'
import EditProductTypeFields from '../views/productTypes/EditProductTypeFields.vue'
import ProductTypeFields from '../views/productTypes/ProductTypeFields'

import PageNotFound from '../views/pages/Page404'

import ChatSms from '../views/leads/chatsms.vue';

import Conversation from '../views/conversation/conversation.vue';
// Views - Buttons






// Views - Pages
const Page404 = () =>
    import ('@/views/pages/Page404')
const Page500 = () =>
    import ('@/views/pages/Page500')
const Login = () =>
    import ('@/views/pages/Login')
const ThankYou = () =>
    import ('@/views/pages/ThankYou')
const Home = () =>
    import ('@/views/pages/home')
const Visitor = () =>
    import ('@/views/pages/visitor')
const CompanySelection = () =>
    import ('@/views/Auth/CompanySelection')
const SelectProductType = () =>
    import ('@/views/Auth/SelectProductType')
const Register = () =>
    import ('@/views/pages/Register')

// Auth
const OwnersRegistration = () =>
    import ('@/views/Auth/OwnersRegistration')

// Users
const Users = () =>
    import ('@/views/users/Users')
const User = () =>
    import ('@/views/users/User')

// Plugins
const Draggable = () =>
    import ('@/views/plugins/Draggable')
const Calendar = () =>
    import ('@/views/plugins/Calendar')
const Spinners = () =>
    import ('@/views/plugins/Spinners')

// Views - UI Kits
const Invoice = () =>
    import ('@/views/apps/invoicing/Invoice')
const Compose = () =>
    import ('@/views/apps/email/Compose')
const Inbox = () =>
    import ('@/views/apps/email/Inbox')
const Message = () =>
    import ('@/views/apps/email/Message')

Vue.use(Router)

export default new Router({

    root: '/app',
    mode: 'history', // https://router.vuejs.org/api/#mode
    linkActiveClass: 'open active',
    scrollBehavior: () => ({ y: 0 }),
    routes: [{
            path: '/',
            redirect: '/home',
            name: 'Home',
            component: TheContainer,
            children: [{
                    path: 'dashboard',
                    name: 'Dashboard',
                    component: Dashboard,
                    meta: {
                        auth: true,
                        company: true,
                        title: 'Dashboard',
                        permission: ['admin', 'owner', 'business_admin', 'business_user']
                    }
                },
                {
                    path: '/companies',
                    name: 'Companies',
                    component: Companies,
                    meta: {
                        auth: true,
                        company: true,
                        title: 'Dashboard',
                        permission: ['admin', 'business_admin']
                    }
                },

                {
                    path: '/edit-company-user/:id',
                    name: 'Create Company User',
                    component: EditCompanyUser,
                    meta: {
                        auth: true,
                        company: true,
                        title: 'Dashboard',
                        permission: ['admin', 'business_admin']
                    }
                },
                {
                    path: '/create-company-user/:id',
                    name: 'Create Company User',
                    component: CreateCompanyUser,
                    meta: {
                        auth: true,
                        company: true,
                        title: 'Dashboard',
                        permission: ['admin', 'business_admin']
                    }
                },
                {
                    path: '/company-users/:id',
                    name: ' Company Users',
                    component: CompanyAdmins,
                    meta: {
                        auth: true,
                        company: true,
                        title: 'Dashboard',
                        permission: ['admin', 'business_admin']
                    }
                },
                {
                    path: '/create-company',
                    name: 'Create Company',
                    component: CreateCompany,
                    meta: {
                        auth: true,
                        company: true,
                        title: 'Dashboard',
                        permission: ['admin', 'business_admin']
                    }
                },
                /*{
                    path: '/create-product-types',
                    name: 'Create Product Types',
                    component: CreateProductTypes,
                    meta: {
                        auth: true,
                        company: true,
                        title: 'Dashboard',
                        permission: ['admin']
                    }
                },*/
                {
                    path: '/product-types',
                    name: 'Product Types',
                    component: ProductTypes,
                    meta: {
                        auth: true,
                        company: true,
                        title: 'Dashboard',
                        permission: ['admin']
                    }
                },
                {
                    path: '/product-types-fields/:id',
                    name: 'Product Type Fields',
                    component: ProductTypeFields,
                    meta: {
                        auth: true,
                        company: true,
                        title: 'Dashboard',
                        permission: ['admin']
                    }
                },
                {
                    path: '/create-product-type-fields/:id',
                    name: 'Create Product Type Fields',
                    component: CreateProductTypeFields,
                    meta: {
                        auth: true,
                        company: true,
                        title: 'Dashboard',
                        permission: ['admin']
                    }
                },
                {
                    path: '/embed',
                    name: 'Emabed',
                    component: Embed
                },

                {
                    path: '/conversation',
                    name: 'Conversation',
                    component: Conversation,
                    meta: {
                        auth: true,
                        company: true,
                        title: 'Dashboard',
                        permission: ['admin', 'business_admin', 'business_user']
                    }
                },
                {
                  path: '/search-keywords',
                  name: 'SearchKeywords',
                  component: SearchKeywords,
                  meta: {
                    auth: true,
                    company: true,
                    title: 'Dashboard',
                    permission: ['admin']
                  }
                },
                {
                    path: '/chat-leads',
                    name: 'Lead',
                    component: Lead,
                    meta: {
                        auth: true,
                        company: true,
                        title: 'Dashboard',
                        permission: ['admin', 'business_admin', 'business_user']
                    }
                },
                {
                    path: '/sms-chat-leads/:id',
                    name: 'ChatSms',
                    component: ChatSms,
                    meta: {
                        auth: true,
                        company: true,
                        title: 'Dashboard',
                        permission: ['admin', 'business_admin', 'business_user']
                    }
                },
                {
                    path: '/owner-buyer-chat/:id',
                    name: 'Owner Buyer chat',
                    component: OwnerBuyer,
                    meta: {
                        auth: true,
                        company: true,
                        title: 'Dashboard',
                        permission: ['admin', 'business_admin', 'business_user']
                    }
                },


                {
                    path: '/buyers',
                    name: 'Buyers',
                    component: Buyer,
                    meta: {
                        auth: true,
                        company: true,
                        title: 'Dashboard',
                        permission: ['admin', 'business_user']
                    }
                },
                {
                    path: '/products/:slug',
                    name: 'Product',
                    component: Product,
                    meta: {
                        auth: true,
                        company: true,
                        title: 'Dashboard',
                        permission: ['owner', 'admin', 'business_user']
                    }
                },
                {
                    path: '/add-products/:slug',
                    name: 'Add Product',
                    component: AddProduct,
                    meta: {
                        auth: true,
                        company: true,
                        title: 'Dashboard',
                        permission: ['owner']
                    }
                },


                {
                    path: '/all-products',
                    name: 'Products',
                    component: AllProduct,
                    meta: {
                        auth: true,
                        title: 'All Product',
                        permission: ['owner', 'admin', 'business_user']
                    }
                },
                {
                    path: '/all-products/:id',
                    name: 'AllProducts',
                    component: CompanyAllProducts,
                    meta: {
                        auth: true,
                        title: 'All Product',
                        permission: ['admin']
                    }
                },
                {
                    path: '/owners',
                    name: 'Owners',
                    component: Owner,
                    meta: {
                        auth: true,
                        company: true,
                        title: 'Dashboard',
                        permission: ['admin', 'business_user']
                    }
                },
                {
                    path: 'charts',
                    name: 'Charts',
                    component: Charts
                },

                /*{

                    path: '/create-product-types',
                    name: 'Create Product Types',
                    meta: {
                        auth: true,
                        company: true,
                        title: 'Dashboard',
                        permission: ['admin']
                    },
                    component: CreateProductTypes
                },*/
                {
                    path: '/product-types',
                    name: 'Product Types',
                    component: ProductTypes,
                    meta: {
                        auth: true,
                        company: true,
                        title: 'Dashboard',
                        permission: ['admin']
                    }
                },
                {
                    path: '/product-types-fields/:id',
                    name: 'Product Type Fields',
                    component: ProductTypeFields,
                    meta: {
                        auth: true,
                        company: true,
                        title: 'Dashboard',
                        permission: ['admin']
                    }
                },
                {
                    path: '/create-product-type-fields/:id',
                    name: 'Create Product Type Fields',
                    component: CreateProductTypeFields,
                    meta: {
                        auth: true,
                        company: true,
                        title: 'Dashboard',
                        permission: ['admin']
                    }
                },
                {
                    path: '/edit-product-type-fields/:id',
                    name: 'Edit Product Type Fields',
                    component: EditProductTypeFields,
                    meta: {
                        auth: true,
                        company: true,
                        title: 'Dashboard',
                        permission: ['admin']
                    }
                },
                {
                    path: '/404',
                    name: 'Page404',
                    component: PageNotFound,
                    meta: {
                        auth: true,
                        title: 'Page Not Found',
                    }
                },

                {
                    path: '/404',
                    name: 'Page404',
                    component: PageNotFound,
                    meta: {
                        auth: true,
                        title: 'Page Not Found',
                    }

                },

                {
                    path: 'users',
                    meta: { label: 'Users' },
                    component: {
                        render(c) { return c('router-view') }
                    },
                    children: [{
                            path: '',
                            component: Users,
                        },
                        {
                            path: ':id',
                            meta: { label: 'User Details' },
                            name: 'User',
                            component: User,
                        },
                    ]
                },



            ]
        },
        {
            path: '/pages',
            redirect: '/pages/404',
            name: 'Pages',
            component: {
                render(c) { return c('router-view') }
            },
            children: [{
                    path: '404',
                    name: 'Page404',
                    component: Page404
                },
                {
                    path: '500',
                    name: 'Page500',
                    component: Page500
                },

                {
                    path: '/owners-signup',
                    name: 'SelectProductType',
                    component: SelectProductType,
                    meta: {
                        auth: false,
                        title: 'Select Category',
                    }
                },

                {
                    path: '/select-company',
                    name: 'CompanySelection',
                    component: CompanySelection,
                    meta: {
                        auth: true,
                        company: false,
                        title: 'Dashboard',
                        permission: ['owner']
                    }
                },

                {
                  path: '/thank-you',
                  name: 'thankyou',
                  component: ThankYou,
                  meta: {
                    auth: false,
                    title: 'Thank you',
                  }
                },


                {
                    path: '/login',
                    name: 'Login',
                    component: Login,
                    meta: {
                        auth: false,
                        title: 'Login',
                    }
                },


                {
                    path: '/login/:slug',
                    name: 'login',
                    component: Login,
                    meta: {
                        auth: false,
                        title: 'Login',
                    }
                },
                {
                    path: '/home',
                    name: 'home',
                    component: Home,
                    meta: {
                        auth: false,
                        title: 'home',
                    }
                },
                {
                    path: '/home/:slug',
                    name: 'Visitor',
                    component: Visitor,
                    meta: {
                        auth: false,
                        title: 'visitor',
                    }
                },

                {
                    path: '/owners-signup/:slug',
                    name: 'OwnersSignup',
                    component: OwnersRegistration,
                    meta: {
                        auth: false,
                        title: 'Owenrs Registration',
                    }
                },



            ]
        }
    ]
})
