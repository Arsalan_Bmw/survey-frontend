export default [
    {
        _name: 'CSidebarNav',
        _children: [{
                _name: 'CSidebarNavItem',
                name: 'Dashboard',
                to: '/dashboard',
                icon: 'cil-speedometer',
                badge: {
                    color: 'primary',
                    text: 'NEW'
                }
            },

            {
                _name: 'CSidebarNavItem',
                name: 'Company',
                to: '/companies',
                icon: 'cil-puzzle'
            },

            {
                _name: 'CSidebarNavItem',
                name: 'Embeded form',
                to: '/embed',
                icon: 'cil-pencil'
            },

            {
                _name: 'CSidebarNavItem',
                name: 'Chat Leads',
                to: '/chat-leads',
                icon: 'cil-envelope-open'
            },

        ]
    }
]
