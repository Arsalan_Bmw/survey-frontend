export default [

    {
        _name: 'CSidebarNav',
        _children: [{
                _name: 'CSidebarNavItem',
                name: 'Dashboard',
                to: '/dashboard',
                icon: 'cil-speedometer',
                badge: {
                    color: 'primary',
                    text: 'NEW'
                }
            },
            {
                _name: 'CSidebarNavDropdown',
                name: 'Companies Management',
                route: '/base',
                icon: 'cil-puzzle',
                items: [{
                        name: 'Companies ',
                        to: '/companies',
                        icon: 'cil-puzzle'
                    },
                    {
                        name: 'Create Company',
                        to: '/create-company',
                        icon: 'cil-puzzle'
                    },





                ]
            },

            {
                _name: 'CSidebarNavDropdown',
                name: 'Product Types',
                route: '/base',
                icon: 'cil-puzzle',
                items: [
                   /* {
                        name: 'Create Product Types',
                        to: '/create-product-types',
                        icon: 'cil-puzzle'
                    },*/
                    {
                        name: 'View Product Types',
                        to: '/product-types',
                        icon: 'cil-puzzle'
                    },

                ]
            },


            {
                _name: 'CSidebarNavItem',
                name: 'Owners',
                to: '/owners',
                icon: 'cil-user'
            },
            {
                _name: 'CSidebarNavItem',
                name: 'Buyers',
                to: '/buyers',
                icon: 'cil-user'
            },

            {
              _name: 'CSidebarNavItem',
              name: 'Products',
              to: '/all-products',
              icon: 'cil-puzzle'
            },

            {
                _name: 'CSidebarNavItem',
                name: 'Conversation',
                to: '/conversation',
                icon: 'cil-envelope-open'
            },
            {
                _name: 'CSidebarNavItem',
                name: 'Chat Leads',
                to: '/chat-leads',
                icon: 'cil-envelope-open'
            },
            {
                _name: 'CSidebarNavItem',
                name: 'Search Keywords',
                to: '/search-keywords',
                icon: 'cil-envelope-open'
            },



        ]
    }
]
