export default [

    {
        _name: 'CSidebarNav',
        _children: [{
                _name: 'CSidebarNavItem',
                name: 'Dashboard',
                to: '/dashboard',
                icon: 'cil-speedometer',
                badge: {
                    color: 'primary',
                    text: 'NEW'
                }
            },

            {
                _name: 'CSidebarNavItem',
                name: 'Buyers',
                to: '/buyers',
                icon: 'cil-user'
            },
            {
                _name: 'CSidebarNavItem',
                name: 'Products',
                to: '/all-products',
                icon: 'cil-puzzle'
            },



            {
                _name: 'CSidebarNavItem',
                name: 'Owners',
                to: '/owners',
                icon: 'cil-user'
            },

        ]
    }
]
