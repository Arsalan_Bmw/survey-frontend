import Cookie from 'js-cookie';

export const state = () => ({
    user:{},
    isSendVerification: false,
    ownerRegistration: true,
    isOwnerAvailability: false,
    isOwnerProduct: false,
    isOwnerDemographic: false
})

export const mutations = {
    /**
     * Set user after register/login
     *
     * @param {*} state
     * @param {*} user
     * @return {void}
     */
    setAuth(state, user){
        state.user = user;
    },
    /**
     * Change state isSendVerification
     *
     * @param state
     * @param {boolean} payload
     * @return {void}
     */
    changeIsSendVerification(state, payload) {
        state.isSendVerification = payload;
    },
    /**
     * Change state ownerRegistration
     *
     * @param state
     * @param {boolean} payload
     * @return {void}
     */
    changeOwnerRegistration(state, payload) {
        state.ownerRegistration = payload;
    },
    /**
     * Change state isOwnerCellPhoneVerified
     *
     * @param state
     * @param {boolean} payload
     * @return {void}
     */
    changeOwnerAvailability(state, payload) {
        state.isOwnerAvailability = payload;
    },
    /**
     * Change state isOwnerProduct
     *
     * @param state
     * @param {boolean} payload
     * @return {void}
     */
    changeOwnerProduct(state, payload) {
        state.isOwnerProduct = payload;
    },
    /**
     * Change state isOwnerDemographic
     *
     * @param state
     * @param {boolean} payload
     * @return {void}
     */
    changeOwnerDemographic(state, payload) {
        state.isOwnerDemographic = payload;
    }
}

export const actions = {
    /**
     * Fetch current authenticate user
     *
     * @return {void}
     */
    async fetchUser({commit}) {
        await this.$axios.get('/api/user').then(res => {
            commit('setAuth',res.data)
        }).catch()
    },
    /**
     * Logout user, delete token and user on cookie
     *
     * @param {*} commit
     * @return {void}
     */
    async logout({commit, dispatch}) {
        await this.$axios.post('/api/v1/auth/logout').then(res => {
            dispatch('removeCookie');
            location.href = '/';
        }).catch();
    },
    /**
     * Make login request to api
     *
     * @param {*} payload
     * @return {void}
     */
    async login({dispatch,commit}, payload) {
        await this.$axios.post('/api/v1/auth/login',payload).then(res => {
          console.log(res);
            dispatch('authenticate', res);
             $nuxt.$router.push('/home');
        }).catch(err => {
            commit('errors/add',err, {root: true});
        })
    },
    /**
     * set user to state.auth, set user and octoken cookie and set Token
     *
     * @param {*} param0
     * @param {*} res
     * @return {void}
     */
    authenticate({commit, dispatch}, res) {

        const user = res.data.data.user;

        const token = res.data.data.token;

        commit('setAuth',user);
        dispatch('setUserCookie', user);
        Cookie.set('octoken',token, { expires: 365});
        this.$axios.setToken(token,'Bearer');
        this.$axiosAuth.defaults.headers.common['Authorization'] = `Bearer ${token}`;
        this.$axiosGQL.defaults.headers.common['Authorization'] = `Bearer ${token}`;
    },
    /**
     * remove Cookie
     *
     * @return {void}
     */
    removeCookie({commit}) {
        commit('setAuth',null);
        Cookie.remove('octoken');
        Cookie.remove('oc_user');
    },
    /**
     * Set User Cookie
     *
     * @param res
     * @return {void}
     */
    setUserCookie({},user) {
        Cookie.set('oc_user', user, { expires: 365});
    },
    /**
     * Verify user phone number
     */
    async verifyPhone({commit, dispatch}, sms_code) {
        try {
            const { data } = await this.$axios.post('/api/v1/register/companies/verify', {sms_code});
            $nuxt.$router.push('/login');

            UIkit.notification('Successfully register, you can continue to login');

            return Promise.resolve(data);
        } catch (err) {
            commit('errors/add', err, {root: true});
            return Promise.reject(err);
        }
    },
    /**
     * Check token when user is authenticate
     *
     * @return {Promise<void>}
     */
    async checkToken({dispatch, commit}) {
        if(this.$cookies.get('octoken')) {
			// this.$axios.get('/api/v1/auth').then(res => {
      //           commit('setAuth', res.data);
      //       }).catch(err => {
			// 	dispatch('removeCookie');
			// })
		}
    }
}

export const getters = {
    initial(state) {
        if(state.user != null){
            let firstName = state.user.name,
                lastName = state.user.name;
            return `${firstName ? firstName.charAt(0) : ''}${lastName ? lastName.charAt(0) : ''}`
        }
        return;
    }
}
