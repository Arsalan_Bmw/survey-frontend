export const state = () => ({
    all:[]
});

export const mutations = {
    index(state, payload) {
        state.all = payload;
    },
    /**
     * Push contact list to state all
     *
     * @param state
     * @param payload
     * @return {void}
     */
    pushContactList(state, payload) {
        state.all.push(payload);
    }
}

export const actions = {
    index({commit}, slug) {
        this.$axios.get(`/api/v1/companies/${slug}/contact_lists`).then(res => {
            commit('index', res.data.data);
        }).catch(err => {

        })
    }
}