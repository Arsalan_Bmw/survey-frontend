export const actions = {
    /**
     * Fetch product types from api
     *
     * @param {*} commit
     */
    async index({commit}) {
        try {
            const { data } = await this.$axios.get('/api/v1/published-product-types');
            return Promise.resolve(data);
        } catch (err) {
            return Promise.reject(err);
        }
    }
}