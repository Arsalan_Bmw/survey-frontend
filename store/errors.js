export const state = () => ({
    all:{},
    message:''
})

export const mutations = {
    /**
     * Add errors to state.all
     *
     * @param state
     * @param err
     * @return void
     */
    add(state,err) {
        if(err.response.status && err.response.status == 422) {
            if(err.response.data && err.response.data.errors) {
                if(typeof err.response.data.errors == 'string') {
                    state.message = err.response.data.errors;
                }
                state.all = err.response.data.errors;
            }

            if(err.response.data && err.response.data.flash) {
                UIkit.notification({
                    message: err.response.data.flash,
                    pos:'bottom-right',
                    status: 'danger'
                })
            }
        }
    },
    /**
     * Remove errors from state.all
     *
     * @param state
     * @return void
     */
    remove(state) {
        state.all = {};
        state.message = ''
    }
}

export const getters = {
    $errors(state) {
        return {
            has:(field) => state.all[field] ? true :false,
            first:(field) => state.all[field][0] || '',
            message(){
                return state.message
            }
        }
    },
}