export const actions = {
    /**
     * Add flash message using UIkit Notification
     * @param {*} param
     * @param {*} payload
     */
    add({}, payload) {
        UIkit.notification(payload);
    }
}