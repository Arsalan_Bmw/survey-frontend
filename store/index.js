export const actions = {
    nuxtServerInit({commit}, {app, req}) {
        let user = null;
        try {
            user = app.$cookies.get('oc_user');
            commit('auth/setAuth',user,{root:true});
        } catch (err) {

        }
    }
}