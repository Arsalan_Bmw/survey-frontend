require('dotenv').config();

module.exports = {
  mode: 'universal',
  /*
   ** Headers of the page
   */
  head: {
    title: 'My awesome project', // Other meta information
    script: [
      { hid: 'stripe', src: 'https://js.stripe.com/v3/', defer: true }
    ],

    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: process.env.npm_package_description || ''
      }
    ],

    link: [

      {
        rel:'stylesheet',
        href: 'https://unpkg.com/vue-rate-it/dist/cdn/vue-rate-it.min.js'
      },
      {
        rel: 'stylesheet',
        href: 'https://unpkg.com/vue-rate-it/dist/font-awesome-rating.min.js'
      },
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href:'https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700&display=swap'},
    ],
    script: [
      { src: '/js/style.js', body: true },
      {
        src: 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDt5wjyMwrXH7skTyPyFn-JoLqh_pO5YZU&libraries=places'
      }
    ]
  },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#f56565' },
  /*
   ** Global CSS
   */
  css: [],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    '~/plugins/components.js',
    '~/plugins/errors.js',
    '~/plugins/axios.js',
    '~/plugins/global-store.js',
    '~/plugins/string.js',
    '~/plugins/wizard.js',
    '~/plugins/oc-route.js',
    '~/plugins/object.js',
    { src: '~plugins/vue-highcharts.js', ssr: false },
    {src: '~/plugins/quill.js', ssr: false},
    {src: '~/plugins/moment.js', ssr: false},
    {src: '~/plugins/flatpickr.js', ssr: false},
    { src: '~/plugins/geo-map.js', ssr: false},
    { src: '~/plugins/paypal.js', ssr: false },

    { src: '~/plugins/loadash.js', ssr: false }
  ],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    // '@nuxtjs/eslint-module',
    // Doc: https://github.com/nuxt-community/nuxt-tailwindcss
    '@nuxtjs/tailwindcss',
    '@nuxtjs/router-extras',
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/pwa',
    ['nuxt-stripe-module', {
      publishableKey: 'pk_test_51HFMS0I7k90FdVQTJeaBbcEvCWr6wqW8Q8uL1eM4EnUq2woqctdz42g5QEJR7ivy1IO9byNZeJb5Wgyzw4dty7Ge00Au0b45Nl',
    }],
    // Doc: https://github.com/nuxt-community/dotenv-module
    '@nuxtjs/dotenv',
    '@nuxtjs/proxy',
    'cookie-universal-nuxt',
  ],
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {
    baseURL: process.env.BASE_URL,
    proxy: true,
    headers: {
      common: {
        'Accept': 'application/json',
        'Content-Type':'application/json',
        'Authorization': null
      },
    }
  },
  /*
   ** Build configuration
   */
  build: {
    vendor: [
      'vue-paypal-checkout'
    ],
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {}
  },
  server: {
    port: process.env.APP_PORT || 3000
  },
  // serverMiddleware: ['~/oauth/oauth.js'],
  /**
   * For proxy to API,
   */
  proxy: {
    '/api': {
      target: process.env.API_URL || 'http://localapi',
      pathRewrite: {
        '^/api' : '/api'
      }
    }
  },
  /**
   * Vue Router
   */
  router: {
    linkActiveClass: 'active',
    linkExactActiveClass: 'active',
  },
}
